// Angular
import { Component, Input, Output, EventEmitter } from '@angular/core'; // tslint:disable-line

// Ionic
import { NavParams } from 'ionic-angular';

@Component({
	selector: 'header-content',
	templateUrl: 'header-content.component.html'
})
export class HeaderContentComponent {
    constructor() {
    }

}